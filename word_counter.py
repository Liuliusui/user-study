import sys
from collections import Counter
import argparse

def count_words(file_path):
    try:
        with open(file_path, 'r', encoding='utf-8') as file: 
            text = file.read()
            words = text.split()
            return len(words)
    except FileNotFoundError:
        return "File not found."

def count_word_frequencies(file_path):
    try:
        with open(file_path, 'r', encoding='utf-8') as file:
            text = file.read()
            words = text.split()
            word_frequencies = Counter(words)
            return len(words), word_frequencies
    except FileNotFoundError:
        print("File not found.")
        return None, None

def generate_summary(file_path, top_n=5):
    try:
        _, word_frequencies = count_word_frequencies(file_path)
        if word_frequencies:
            print(f"Top {top_n} Words:")
            for word, freq in word_frequencies.most_common(top_n):
                print(f"{word}: {freq}")
    except FileNotFoundError:
        print("File not found.")
    except Exception as e:
        print(f"An error occurred: {e}")

def main():
    parser = argparse.ArgumentParser(description="Text file analysis tool")
    parser.add_argument("file_path", help="Path to the text file")
    parser.add_argument("mode", choices=["count", "frequencies", "summary"], help="Mode of operation")
    parser.add_argument("--top_n", type=int, default=5, help="Number of top words to show for 'summary' mode")

    args = parser.parse_args()

    if args.mode == "count":
        total_words = count_words(args.file_path)
        print(f"Word count: {total_words}")
    elif args.mode == "frequencies":
        _, word_frequencies = count_word_frequencies(args.file_path)
        print("Word Frequencies:")
        for word, freq in word_frequencies.most_common():
            print(f"{word}: {freq}")
    elif args.mode == "summary":
        generate_summary(args.file_path, args.top_n)

if __name__ == "__main__":
    main()
